import org.junit.Assert;
import org.junit.Test;


public class TestCard {

  @Test
  public void testGetValue(){
    Card c = new Card(Rank.ACE, Suit.SPADES);
    Assert.assertEquals(11, c.getValue());
  }

  @Test
  public void testToString(){
    Card c = new Card(Rank.JACK, Suit.HEARTS);
    Assert.assertEquals("JACK of HEARTS", c.toString());
  }

  @Test
  public void testAceToggle(){
    Card c = new Card(Rank.ACE, Suit.CLUBS);
    Assert.assertEquals(c.getValue(), 11);
    c.setHighAce(false);
    Assert.assertEquals(c.getValue(), 1);
  }

  @Test
  public void testGetSuit(){
    Card c = new Card(Rank.TWO, Suit.CLUBS);
    Assert.assertEquals(c.getSuit(), "CLUBS");
  }
}
