import org.junit.Assert;
import org.junit.Test;

public class TestHand{

  @Test
  public void testHand(){
    Deck d = new Deck();
    Hand h1 = new Hand(d.draw2());
    Hand h2 = new Hand(d.draw(), d.draw());
    Assert.assertNotNull(h1);
    Assert.assertNotNull(h2);
    Assert.assertNotNull(h1.getScore());
  }

  @Test
  public void testGetScore(){
    Hand h = new Hand(new Card(Rank.ACE, Suit.SPADES), new Card(Rank.JACK, Suit.DIAMONDS));
    Assert.assertEquals(h.getScore(), 21);
  }

  @Test
  public void testHit(){
    Hand h = new Hand(new Card(Rank.SEVEN, Suit.SPADES), new Card(Rank.SEVEN, Suit.DIAMONDS));
    h.hit(new Card(Rank.SEVEN, Suit.HEARTS));
    Assert.assertEquals(h.getScore(), 21);
  }

  @Test
  public void testToString(){
    Hand h = new Hand(new Card(Rank.SEVEN, Suit.SPADES), new Card(Rank.SEVEN, Suit.DIAMONDS));
    Assert.assertEquals(h.toString(), "| SEVEN of SPADES | SEVEN of DIAMONDS | ");
  }

  @Test
  public void testDealerHand(){
    Hand h = new Hand(new Card(Rank.SEVEN, Suit.SPADES), new Card(Rank.SEVEN, Suit.DIAMONDS));
    Assert.assertEquals(h.printDealerHand(), "|  X  | SEVEN of DIAMONDS | ");
  }

  @Test
  public void testSplitCheck(){
    Hand h = new Hand(new Card(Rank.SEVEN, Suit.SPADES), new Card(Rank.SEVEN, Suit.DIAMONDS));
    Assert.assertEquals(h.splitCheck(), true);
    h.hit(new Deck().draw());
    Assert.assertEquals(h.splitCheck(), false);
  }

  @Test
  public void testCanDoubleDown(){
    Hand h = new Hand(new Card(Rank.NINE, Suit.SPADES), new Card(Rank.JACK, Suit.DIAMONDS));
    Assert.assertEquals(h.canDoubleDown(), true);
    h.hit(new Deck().draw());
    Assert.assertEquals(h.canDoubleDown(), false);
  }

  @Test
  public void testGetCard(){
    Card c = new Card(Rank.THREE, Suit.DIAMONDS);
    Hand h = new Hand(c, new Card(Rank.KING, Suit.CLUBS));
    Assert.assertEquals(h.getCard(0), c);
  }

  @Test
  public void testAceHandler(){
    Hand h = new Hand(new Card(Rank.ACE, Suit.SPADES), new Card(Rank.JACK, Suit.DIAMONDS));
    Assert.assertEquals(h.aceCheck(), 0);
  }
}
