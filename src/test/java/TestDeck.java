import org.junit.Assert;
import org.junit.Test;
import java.util.List;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class TestDeck {

  @Test
  public void testGetCardsRemaining(){
    Deck d = new Deck();
    Assert.assertEquals(d.getCardsRemaining(), 52);
  }

  @Test
  public void testDraw(){
    Deck d = new Deck();
    Card c1 = d.draw();
    Card c2 = d.draw();
    Card c3 = d.draw();
    Assert.assertNotNull(c3);
    Assert.assertEquals(d.getCardsRemaining(), 49);
  }

  @Test
  public void testPeek(){
    Deck d = new Deck();
    Card c = d.peek();
    Assert.assertNotNull(c);
    Assert.assertEquals(d.cards.size(), 52);
  }

  @Test
  public void testDraw2(){
    Deck d = new Deck();
    List<Card> h = d.draw2();
    Assert.assertNotNull(h);
    Assert.assertEquals(h.size(), 2);
    Card c = h.get(0);
    Assert.assertNotNull(c);
  }

  @Test
  public void testDeckCheck(){
    Deck d = new Deck();
    for(int i = 0; i < 51; i++)
      d.draw();
    Assert.assertEquals(d.cards.size(), 1);
    // check that I can always draw 2
    List<Card> c = d.draw2();
    Assert.assertEquals(c.size(), 2);
    Assert.assertEquals(d.cards.size(), 50);
  }

  @Test
  public void testShuffle(){
    Deck d = new Deck();
    List<Card> l1 = new ArrayList<Card>(5);
    for(int i = 0; i < 5; i ++){
      l1.add(d.peek());
      d.shuffle();
    }
    List<Card> l2 = new ArrayList<Card>(5);
    for(int i = 0; i < 5; i++){
      l2.add(d.peek());
      d.shuffle();
    }
    Assert.assertNotEquals(l1, l2);
  }

  @Test
  public void testLargeDeck(){
    Deck d = new LargeDeck(8);
    Assert.assertEquals(d.cards.size(), 416);
  }
}
