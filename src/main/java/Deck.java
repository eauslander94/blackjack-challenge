import java.util.Random;
import java.util.List;
import java.util.ArrayList;
import java.util.LinkedList;


public class Deck {

  protected List<Card> cards;
  protected int cardsRemaining;

  // Creates new shuffled 52 card deck;
  public Deck(){
    this.cards = new LinkedList<Card>();
    for(Suit s: Suit.values())
      for(Rank r: Rank.values())
        this.cards.add(0, new Card(r, s));
    this.cardsRemaining = 52;
    this.shuffle();
  }

  // Returns card at the top of the deck
  public Card draw(){
    this.deckCheck();
    this.cardsRemaining--;
    return cards.remove(0);
  }

  // Returns list of 2 cards
  public List<Card> draw2(){
    this.deckCheck();
    List<Card> hand = new ArrayList<Card>(2);
    hand.add(this.cards.remove(0));
    hand.add(this.cards.remove(0));
    this.cardsRemaining -= 2;
    return hand;
  }

  // If only one card left, set cards to new reshuffled deck
  private void deckCheck(){
    if(this.cards.size() == 1){
      this.cards = new LinkedList<Card>();
      for(Suit s: Suit.values())
        for(Rank r: Rank.values())
          this.cards.add(0, new Card(r, s));
      this.cardsRemaining = 52;
      this.shuffle();
    }
  }

  // Returns top card without removing it from list
  public Card peek(){
    return this.cards.get(0);
  }

  // Returns number of cards currently in this deck
  public int getCardsRemaining(){
    return this.cardsRemaining;
  }

  // Post: Cards remaining in deck are shuffled
  public void shuffle(){
    List<Card> newList = new LinkedList<Card>();
    Random rand = new Random();
    while(this.cards.size() > 0)
      // Add to front of new list card from random position in cards
      newList.add(0, this.cards.remove(rand.nextInt(this.cards.size())));
    this.cards = newList;
  }
}
