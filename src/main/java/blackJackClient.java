import java.util.Scanner;

public class blackJackClient {

  public static void main(String[]args){
    startGame();
  }


  // Prompts user for number of decks to play with, starts game
  public static void startGame(){

    System.out.println("\nLet's play BlackJack!\nHow many Decks do you wish to "
    + "play with? (1, 2, 4 or 8)");
    Scanner sc = new Scanner(System.in);
    int decks = 0;
    while(sc.hasNext()){
      decks = sc.nextInt();
      // Get number of decks to play with
      if(decks < 9 && decks > 0 && (decks & (decks - 1)) == 0){
        System.out.println("Got it! Do you wish to play in test mode? (1)Yes (2)No");
        // Are we in test mode
        int testNumber = 0;
        boolean test = false;
        while(sc.hasNext()){
          testNumber = sc.nextInt();
          if(testNumber == 1){
            test = true;
            break;
          }
          else if(testNumber == 2){
            test = false;
            break;
          }
          else System.out.println("Please enter 1 or 2");
        }
        System.out.println("Let's Play!");
        Game game = new Game(decks, test);
        game.play();
        break;
      }
      else
        System.out.println("Please enter 1, 2, 4 or 8");
    }
  }

}
