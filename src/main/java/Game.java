import java.util.Scanner;
import java.util.List;
import java.util.ArrayList;

public class Game {

  private Deck deck;
  private Hand userHand;
  private Hand dealerHand;
  private int userMoney = 100;
  private int dealerMoney = 100;
  private int currentBet = 0;
  private List<Hand> splitHands;
  private boolean didSplit = false;;
  private boolean doubleDown = false;
  private boolean testMode = false;


  // Initialize deck with number of decks
  public Game(int decks){
    if(decks > 1)
      this.deck = new LargeDeck(decks);
    else this.deck = new Deck();
  }

  // Constrcutor that allows us to turn on testMode
  public Game(int decks, boolean testMode){
    if(decks > 1)
      this.deck = new LargeDeck(decks);
    else this.deck = new Deck();
    this.testMode = testMode;
  }

  // Game loop - plays the game
  // Postcondition: Game is started and plays until dealer or user reaches $0 or the user surrenders
  public void play(){
    while(this.userMoney > 0 && this.dealerMoney > 0){
      this.setBets();
      this.setHands();
      this.userTurn();
      this.dealerTurn();
      this.determineWinner();
    }
    System.out.println("\nGame over! You have $" + this.userMoney + " and the dealer has $" + this.dealerMoney);
  }


  // user prompted to play a turn
  // Post: userHand is updated based on user moves
  private void userTurn(){
    Scanner sc = new Scanner(System.in);
    while(this.userHand.getScore() < 22){
      this.userHand.aceHandler();
      this.printPrompt();
      switch(sc.next()){
        case "1": {
          Card c = this.deck.draw();
          System.out.println("You drew the " + c);
          this.userHand.hit(c);
          this.userHand.aceCheck();
          break;
        }
        case "2": {
          System.out.println("You've chosen to stand. Your score is " + this.userHand.getScore());
          return;
        }
        case "3": {
          if(this.userHand.canDoubleDown()){
            this.doubleDown = true;
            break;
          }
          else {
            System.out.println("Can't double down");
            break;
          }
        }
        case "4": {
          if(this.userHand.splitCheck() && !this.didSplit){
            this.split();
            return;
          }
          else {
            System.out.println("Can't split");
            break;
          }
        }
        case "5": {
          System.out.println("You've chosen to surrender. Goodbye.");
          System.exit(0);
        }
        default: {
          System.out.println("Invalid Entry. Please enter one of the options above");
          break;
        }
      }
      if(this.doubleDown)
        this.doubleDownBehavior();
    }
    System.out.println("You've busted. \nHand: " + this.userHand + "\nScore: " + this.userHand.getScore());
    return;
  }

  // Post: user & dealer bets set, prints monetary state
  private void setBets(){
    System.out.println("\nNew Hand! You have $" + this.userMoney + " and the dealer has $" + this.dealerMoney);
    this.currentBet = 20;
    this.userMoney -= 10;
    this.dealerMoney -= 10;
  }

  // Post: current pot, as well as user & dealer cotributions, is doubled
  public void doubleBets(){
    this.userMoney -= this.currentBet / 2;
    this.dealerMoney -= this.currentBet / 2;
    this.currentBet *= 2;
    System.out.println("Current pot is now $" + this.currentBet);
  }

  // Post: new hands are drawn
  private void setHands(){
    if(this.testMode)
      this.userHand = this.getTestHand();
    else
      this.userHand = new Hand(this.deck.draw2());
    this.dealerHand = new Hand(this.deck.draw2());
  }

  // Returns hand containing two jacks for testing
  public Hand getTestHand(){
    return new Hand(new Card(Rank.JACK, Suit.CLUBS), new Card(Rank.JACK, Suit.SPADES));
  }

  // Performs double down behavior, prints results
  // Returns: updated hand
  private void doubleDownBehavior(){
    System.out.println("Double Down!");
    Card c = this.deck.draw();
    this.userHand.hit(c);
    this.userHand.aceHandler();
    System.out.println("You drew the " + c + "\nYour score is " + this.userHand.getScore());
  }

  // Postcondition: user hand split into 2, both play & populate splitHands
  private void split(){
    this.splitHands = new ArrayList<Hand>();
    this.didSplit = true;
    Hand split1 = new Hand(this.userHand.getCard(0), this.deck.draw());
    Hand split2 = new Hand(this.userHand.getCard(1), this.deck.draw());
    this.userHand = split1;
    this.userTurn();
    this.splitHands.add(this.userHand);
    this.userHand = split2;
    this.userTurn();
    this.splitHands.add(this.userHand);
  }

  // Returns: true if just 1 split hand beats dealer, false otherwise
  // Postcondition: splitHands nullified
  private boolean splitWin(){
  int dealerScore = this.dealerHand.getScore();
    boolean splitWin = false;
    for(int i = 0; i < this.splitHands.size(); i++)
      if(this.didUserWin(this.splitHands.get(i).getScore()))
        splitWin = true;
    this.didSplit = false;
    return splitWin;
  }

  // Prints prompt for user to make a move
  // Param: hand to be used to print cards & score
  private void printPrompt(){
    if(this.didSplit)
      System.out.println("Split hand!");
    System.out.println("Your hand:   " + this.userHand + "\nDealer hand: " + this.dealerHand.printDealerHand());
    System.out.println("\nPlease make a move. (1)Hit (2)Stand (3)DoubleDown (4)Split (5)Surrender");
  }

  // Goes through logic of the dealer's turn
  // Post: Dealer hand updated after turn plays out
  private void dealerTurn(){
    Hand hand = this.dealerHand;
    System.out.println("\nDealer has:" + hand.printDealerHand());
    while(hand.getScore() < 14){
      hand.hit(this.deck.draw());
      System.out.println("Dealer hit! They now have " + hand.printDealerHand());
    }
    if(hand.getScore() > 22)
      System.out.println("Dealer busted! Score: " + hand.getScore() + "\n" + hand);
    else System.out.println("Dealer stands! Score: " + hand.getScore() + "\n" + hand);
    this.dealerHand = hand;
  }

  // Post: winner is determined, money is exchanged, winner is printed
  private void determineWinner(){
    // Handle splits
    if(this.didSplit){
      this.doubleBets();
      if(this.splitWin())
        this.userWin();
      else
        this.dealerWin();
      return;
    }
    if(this.doubleDown){
      this.doubleBets();
      this.doubleDown = false;
    }
    if(this.didUserWin(this.userHand.getScore()))
      this.userWin();
    else
      this.dealerWin();
  }

  // Returns true if user wins, false if dealer wins
  // Param: user score to play against the dealer
  public boolean didUserWin(int userScore){
    int dealerScore = this.dealerHand.getScore();
    if(userScore > 21 || userScore == dealerScore)  // user bust or tie
      return false;
    if(dealerScore > 21 || userScore > dealerScore) // dealer bust or user is higher w/ neither bust
      return true;
    return false;  // dealer is higher w/ neither bust
  }

  // Post: user receives current bet & results are printed
  private void userWin(){
    this.userMoney += this.currentBet;
    System.out.println("You win $" + this.currentBet + "!");
  }

  // Post: Dealer receives current pot & results are printed
  private void dealerWin(){
    this.dealerMoney += this.currentBet;
    System.out.println("Dealer wins $" + this.currentBet + "...");
  }
}
