import java.util.Scanner;
import java.util.List;
import java.util.ArrayList;

public class Hand {

  private List<Card> cards;

  // Constructor that takes in result from deck object's draw2() method
  // Precondition: cards holds exactly 2 cards
  public Hand(List<Card> h){
    this.cards = h;
  }

  // Constructor that takes in 2 card objects
  public Hand(Card c1, Card c2){
    this.cards = new ArrayList<Card>();
    this.cards.add(c1);
    this.cards.add(c2);
  }

  // Returns score of the hand
  public int getScore(){
    int score = 0;
    for(int i = 0; i < this.cards.size(); i++)
      score += this.cards.get(i).getValue();
    return score;
  }

  // Adds provided card to hand
  public void hit(Card c){
    this.cards.add(c);
  }

  // returns String representation of your cards
  public String toString(){
    String s = "| ";
    for(int i = 0; i < this.cards.size(); i++)
      s += this.cards.get(i).toString() + " | ";
    return s;
  }

  // Hides first card and prints rest of hand
  public String printDealerHand(){
    String s = "|  X  | ";
    for(int i = 1; i < this.cards.size(); i++)
      s += this.cards.get(i) + " | ";
    return s;
  }

  // Checks if hand has 2 cards w/ same value, thus able to split
  // Returns boolean corresponding to legality of split on current hand
  public boolean splitCheck(){
    if(this.cards.size() == 2 && this.cards.get(0).getValue() == this.cards.get(1).getValue())
      return true;
    return false;
  }

  // Returns: true if user can double down, false otherwise
  // MUST CHECK IF USER STOOD ON FIRST TURN
  public boolean canDoubleDown(){
    if(this.cards.size() == 2)
      return true;
    return false;
  }

  // Returns card at specified index in hand
  public Card getCard(int index){
    return this.cards.get(index);
  }

  // TODO move to Game class
  // Post: Checks if there is an ace in the user's hand. If so, prompts for
  //   User to input new value for ace(1 or 11)
  public void aceHandler(){
    if(this.aceCheck() == -1)
      return;
    System.out.println("There's an Ace in you hand! Your hand is: " + this.toString());
    Scanner sc = new Scanner(System.in);
    int input = 0;
    while(true){
      System.out.println("How do you wish to treat this Ace? (1)1 point (11)11 points");
      input = sc.nextInt();
      if(input == 1){
        this.cards.get(this.aceCheck()).setHighAce(false);
        return;
      }
      else if(input == 11){
        this.cards.get(this.aceCheck()).setHighAce(true);
        return;
      }
      System.out.println("Please enter a valid input");
    }
  }

  // Returns index of ace in user's hand, -1 if not present
  public int aceCheck(){
    for(int i = 0; i < this.cards.size(); i++)
      if(cards.get(i).getValue() == 11 || this.cards.get(i).getValue() == 1)
        return i;
    return -1;
  }
}
