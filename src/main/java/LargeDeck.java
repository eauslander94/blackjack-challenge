import java.util.List;

public class LargeDeck extends Deck{

  public LargeDeck(int numberOfDecks){
    for(int i = 1; i < numberOfDecks; i++)  // loop from 1 bc of implicit call to deck constructor
    for(Suit s: Suit.values())
      for(Rank r: Rank.values()){
        this.cards.add(0, new Card(r, s));
        this.cardsRemaining++;
      }
    this.shuffle();
  }

}
