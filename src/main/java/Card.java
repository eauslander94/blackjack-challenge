

public class Card {

  private Suit suit;
  private Rank rank;
  private boolean aceHigh;

  public int value;

  // Precondition: input value must be between 0 and 51 inclusive
  // Postcondition: Card created with suit & value derived from input value
  public Card(int value){
    if(value < 0 || value > 52)
      return;
    this.value = value;
  }

  public Card(Rank rank, Suit suit){
    this.rank = rank;
    this.suit = suit;
    this.aceHigh = true;
  }

  // Returns String name of the card's suit, 1st letter capitalized
  public String getSuit(){
    return "" + this.suit;
  }

  // Returns blackJack point value of the card
  public int getValue() {
    if(this.rank == Rank.ACE && this.aceHigh)
      return 11;
    return this.rank.getValue();
  }

  // Postcondition: Ace is set to either 1 or eleven based on input Parameter
  // Param: true - ace becomes 11, false - ace becomes 1
  public void setHighAce(boolean aceValue){
    this.aceHigh = aceValue;
  }

  // Returns string representation of Card
  public String toString(){
    return this.rank + " of " + this.suit;
  }
}
